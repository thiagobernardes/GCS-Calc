#include <stdio.h>

float subtract (float number1, float number2){
  float resultSubtract = 0.0;
  resultSubtract = number1 - number2;
  return resultSubtract;
}

float sum(float a, float b) {
  return a + b;
}

float multiplier(float num1, float num2) {
  return num1 * num2;
}

int main(int argc, char const *argv[]) {

  float num1, num2, result;
  char operation;

  scanf("%f %c %f", &num1, &operation, &num2);

  switch(operation) {
    case '+':
      result = sum(num1, num2);
    break;
    case '-':
      result = subtract(num1, num2);
    break;
    case '*':
      result = multiplier(num1, num2);
    break;
    default:
      result = sum(num1, num2);
    break;

  }

  printf("%.2f\n", result);
  return 0;
}
